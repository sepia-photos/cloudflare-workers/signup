# signup

Sepia's backend consists of serverless functions, MongoDB, and object storage. The object storage is hosted on Backblaze B2, and all requests to the API are handled by Cloudflare workers. When necessary, information is passed on to IBM cloud functions which can interface with the database.

This worker handles requests with the /signup endpoint. It accepts basic authentication in the form of a username and a hashed password (password hashing is done client-side) which is passed on to the "signup" IBM cloud function. The IBM cloud function returns a short-lived access token (in the form of a JWT) and a refresh token (in the form of a UUID). The Cloudflare workers sets both of these as cookies.

The access token is used for authorization with other API endpoints while the refresh token is used with the /refresh endpoint to get a new access token after the old one expires.

## Deployment

Right now there is no deployment pipeline configuration setup. To deploy this yourself you either create a Cloudflare Worker and manually paste in the contents of index.js or configure [wrangler](https://developers.cloudflare.com/workers/cli-wrangler/install-update), Cloudflare's cli tool for workers.
