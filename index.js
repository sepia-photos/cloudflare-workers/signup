addEventListener('fetch', event => {
    if (event.request.method === "OPTIONS"){
    event.respondWith(preFlight(event.request))
  }
  else{
    event.respondWith(handleRequest(event.request))
  }
})

/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {
  //apiURL is set as an encrypted environment variable
  const response = await fetch(apiURL, {
    method: "post",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: request.body
  }).then(res => {
    return res;
  });

  const tempresp = new Response(response.body,response)
  const originalBody = await tempresp.json()
  const cookie = originalBody.headers["Set-Cookie"]

  if (cookie != undefined) {
    cookie.forEach(cookie => tempresp.headers.append("set-cookie", cookie));
  }

  const body = JSON.stringify(originalBody.body)
  newresp = new Response(body, {
        status: originalBody.statusCode,
        statusText: originalBody.statusMessage,    
        headers: tempresp.headers
  })

  return newresp;
}

/**
 * Respond to the PreFlight cors Options request
 * @param {Request} request
 */
async function preFlight(request) {
    const response = new Response(request.body, {
        status: 200,
        statusText: "OK",    
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'access-control-allow-origin': '*',
          'access-control-allow-credentials': true,
          'access-control-allow-headers': 'Authorization, Origin, X-Requested-With, Content-Type, Accept, User-Agent',
          'access-control-allow-methods': 'GET, POST, PUT, DELETE, PATCH, HEAD, OPTIONS'
        }
    })

  return response;
}
